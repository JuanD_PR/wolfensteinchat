# README #

This README explains how to install the NodeJS Server and run the application

### Installing NodeJS ###

* 1) go to https://nodejs.org/
* 2) click on the green "Install" button
* 3) install NodeJS
* 4) check the success of the installation running

```
#!batch

node --version
```
in the command line


### Installing MongoDB ###

* 1) go to https://www.mongodb.org/downloads
* 2) go down to "Current Stable Release"
* 3) select the appropiate version and download it
* 4) install it
* 5) add the "bin" directory to the environment variables
* 6) Check the success of the installation ruuning

```
#!batch

mongo --version
```

### Installing the repository ###

* 1) clone this repo in an empty folder
You will see 1 directory called "web" with the frontend application and a file called "index.js" which is the NodeJS backend application


### Downloading dependencies ###
* 1) open a command line in your application root
* 2) run the command

```
#!batch

npm install
```

### Mounting a local server ###

* 1) open a command line in your application root
* 2) run the command

```
#!batch

node index.js
```

* 3) go to http://127.0.0.1:3000