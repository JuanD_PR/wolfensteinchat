// ----------- SSL INICIALIZATION ----------------
var fs = require("fs");
var privateKey = fs.readFileSync("key.pem", 'utf8');        //load openssl generated privateKey
var certificate = fs.readFileSync('key-cert.pem', 'utf8');  //load openssl generated certificate
var credentials = {key: privateKey, cert: certificate, requestCert: true};     //create credentials object to create ssl
// ----------- SSL INICIALIZATION ----------------

// ----------- MONGO DB INICIALIZATION ----------------
var mongoose = require('mongoose');
require('./lib/user.js')();
require('./lib/message.js')();
mongoose.connect('mongodb://localhost/wolfenstein-chat');

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
  console.log('Mongo is up!!!');
});
// ----------- MONGO DB INICIALIZATION ----------------


// ----------- APP INICIALIZATION ----------------
var connectedUsers = []

var express = require('express'),       // call express
    app = express(),                     // define our app using express
    https = require('https').Server(credentials, app),
    io = require('socket.io').listen(https),
    bodyParser = require('body-parser');           //Get the https module

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use('/web', express.static(__dirname + '/web'));
// ----------- APP INICIALIZATION ----------------

// ----------- APP ROUTING ----------------
app.get('/', function(req, res) {
  	res.sendFile(__dirname + '/web/login.html');
});

app.get('/chat', function(req, res) {
  	res.sendFile(__dirname + '/web/index.html');
});

app.post('/login', function(req, res) {
	var usernameSent = req.body.username;
	var passwordSent = req.body.password;
  	var promise = getUser(usernameSent)

  	promise.then(function(usr) {
      	if(usr) {
          	var compareCallBack = function(er, isMatch) {
              	if(isMatch) {
                 	console.log('------- User loggued in -------- ', usr);
                  	res.sendStatus(200);                  	
              	} else {
                  	console.log('User not found: ', usr);
                  	res.sendStatus(400);
              	}
          	}

          	usr.comparePassword(passwordSent, compareCallBack);
      	} else {
          	console.log('User not found: ', usr);
          	res.sendStatus(400);
      	}
        
  	})
});
// ----------- APP ROUTING ----------------

// ----------- SOCKET IO ----------------
io.on('connection', function(socket) {

  // Listening the event sent by the client
  socket.on('SERVER_SET_USERNAME', function(userName) {
      socket.loggedUserName = userName;

      if (connectedUsers.indexOf(userName) < 0) {
      	connectedUsers.push(userName);
      }

      io.emit('CLIENT_CONNECTED_USERS', connectedUsers);
      
  });

  // Listening the event sent by the client
  socket.on('SERVER_CHAT_MESSAGE', function(messageSent) {
  	messageSent.username = socket.loggedUserName;

	// Send the message to all the users
    io.emit('CLIENT_CHAT_MESSAGE', messageSent);

    // Saving the sent message into a Mongo Document
    addMessage(messageSent);
  });

  // Listening the event sent by the client
  socket.on('SERVER_GET_LAST_MESSAGES', function() {
      var promise = getLastMessages(50);

      promise.then(function(messages) {
          io.emit('CLIENT_GET_LAST_MESSAGES', messages);
      });
    
  });

  socket.on('disconnect', function () {
	var index = connectedUsers.indexOf(socket.loggedUserName);
	if (index >= 0) {
		connectedUsers.splice(index, 1);
	}

	io.emit('CLIENT_CONNECTED_USERS', connectedUsers);
	
  });

});


// ----------- SOCKET IO ----------------

// ----------- SERVER INICIALIZAITION ----------------
try {
    //https server listen all request of 8089 port
    https.listen(8089, function () {
        "use strict";
        console.log('Magic SSL happens on port ' + 8089);
    });
}
catch(e){
    console.error(e);
}
// ----------- SERVER INICIALIZAITION ----------------


// ----------- UTIL FUNCTIONS ----------------
var getLastMessages = function(quantity) {
    var MessageDocument = mongoose.model('Message');	
  	return MessageDocument.find({}).limit(quantity).exec(); //returns a promise
};

var addMessage = function(messageSent) {
    var MessageDocument = mongoose.model('Message');
    var message = new MessageDocument(messageSent);
    return message.save();
};

var getUser = function(username) {
    var UserDocument = mongoose.model('User');
    return UserDocument.where({ username: username}).findOne().exec(); //returns a promise
}
// ----------- UTIL FUNCTIONS ----------------