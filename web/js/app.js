(function() {

  var app = angular.module('wolfensteinChat', []);

  app.service('loginService', function($http) {

    this.performLogin = function(username, password) {
      return $http({
        method: 'POST',
        url: '/login',
        data: {
          'username' : username,
          'password' : password
        }
      }).error(function(){
        alert('Wrong username:password');
      });
    };

  });

  app.service('cookieService', function() {

    this.setCookie = function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    this.getCookie = function(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i=0; i<ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0)==' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }

    this.deleteCookie = function(cname) {
      document.cookie = cname + "=; expires=Thu, 01 Jan 1970 00:00:00 UTC"; 
    }

  });

  app.factory('socket', function ($rootScope) {
    var socket = io.connect('https://127.0.0.1', {secure: true, port: 8089});
    return {
      on: function (eventName, callback) {
        socket.on(eventName, function() {
          var args = arguments;
          $rootScope.$apply(function() {
            callback.apply(socket, args);
          });
        });
      },
      emit: function (eventName, data, callback) {
        socket.emit(eventName, data, function () {
          var args = arguments;
          $rootScope.$apply(function() {
            if (callback) {
              callback.apply(socket, args);
            }
          });
        })
      }
    };
  });

  app.controller('ChatController', function($scope, socket, cookieService) {

    if (cookieService.getCookie("username") == '') {
      alert("You are not logged in");
      window.location = "/";
    }

    $scope.message = {};
    $scope.messages = [];
    $scope.connectedUsers = [];

    socket.emit('SERVER_SET_USERNAME', cookieService.getCookie("username"));
    socket.emit('SERVER_GET_LAST_MESSAGES');

    $scope.sendMessage = function() {
      var messageToSent = {};
      
      messageToSent.message = $scope.message.text;
      messageToSent.datetime = new Date();

      socket.emit('SERVER_CHAT_MESSAGE', messageToSent);

      $scope.message = {};
    };

    socket.on('CLIENT_CHAT_MESSAGE', function(messageReceived) {
      $scope.messages.push(messageReceived);
      setTimeout(function() {
        var objDiv = document.getElementById("chatDisplay");
        objDiv.scrollTop = objDiv.scrollHeight;
      }, 500);
      
    });

    socket.on('CLIENT_GET_LAST_MESSAGES', function(lastMessages) {
      $scope.messages = lastMessages;
    });

    socket.on('CLIENT_CONNECTED_USERS', function(connectedUsers) {
      console.log(connectedUsers);
      $scope.connectedUsers = connectedUsers;
    });

  });

  app.controller('LoginController', function($scope, loginService, cookieService) {
      $scope.loginObj = {};

      $scope.login = function() {
        loginService.performLogin($scope.loginObj.username, $scope.loginObj.password).then(function(dataResponse) {
          cookieService.setCookie("username", $scope.loginObj.username, undefined);
          window.location = "/chat";
        });
      };
  });

})();