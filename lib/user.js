var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    bcrypt = require('bcrypt'),
    SALT_WORK_FACTOR = 10;

var UserSchema = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true }
});

UserSchema.pre('save', function(next) {
    var user = this;

    // only hash the password if it has been modified (or is new)
    if (!user.isModified('password')) return next();

    // generate a salt
    bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
        if (err) return next(err);

        // hash the password using our new salt
        bcrypt.hash(user.password, salt, function(err, hash) {
            if (err) return next(err);

            // override the cleartext password with the hashed one
            user.password = hash;
            next();
        });
    });
});

UserSchema.methods.comparePassword = function(candidatePassword, callback) {
    bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
        if (err) return callback(err);
        callback(null, isMatch);
    });
};
var User = mongoose.model('User', UserSchema);
var promise = User.count({});
promise.then(function(countUsers){
    console.log('countUsers' + countUsers);
    if (countUsers === 0) {
        console.log('entre a crear usuarios por primera vez');
        var user1 = new User({'username':'JuanD', 'password': '123456'});
        user1.save();
        var user2 = new User({'username':'Javergui', 'password': 'asdasdasd'});
        user2.save();
        var user3 = new User({'username':'Gabriel', 'password': 'password1'});
        user3.save();
        var user4 = new User({'username':'Juan', 'password': 'password1'});
        user4.save();
        var user5 = new User({'username':'Sabrina', 'password': 'password1'});
        user5.save();
        var user6 = new User({'username':'Javier', 'password': 'password1'});
        user6.save();
    }
});
module.exports = User;