// import the necessary modules
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Nodejs encryption with CTR
var crypto = require('crypto'),
    algorithm = 'aes-256-ctr',
    password = 'wolfenstein%&/2123=';

var MessageSchema = new Schema({
    message: { type: String, required: true },
    datetime: { type: String, required: true },
    username: { type: String, required: true }
});

MessageSchema.methods.encrypt = function(text) {
	var cipher = crypto.createCipher(algorithm,password);
	var crypted = cipher.update(text,'utf8','hex');
	crypted += cipher.final('hex');
	return crypted;
};

MessageSchema.methods.decrypt = function(text) {
	console.log('texto a decodificar: '+text);
    var decipher = crypto.createDecipher(algorithm,password);
	var dec = decipher.update(text,'hex','utf8')
	dec += decipher.final('utf8');
	return dec;
};


MessageSchema.pre('save', function(next) {
    var message = this;
    message.message = message.encrypt(message.message);
    console.log('pre save' + message);
    next();
});

MessageSchema.post('init', function(next) {
    var message = this;
    message.message = message.decrypt(message.message);
    console.log('post init' + message);
});

var message = mongoose.model('Message', MessageSchema);
module.exports = message;